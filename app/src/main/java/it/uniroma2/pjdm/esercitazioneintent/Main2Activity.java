package it.uniroma2.pjdm.esercitazioneintent;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {
    private final int ADDR_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void pickAddress(View view) {
        Intent addrIntent = new Intent();
        addrIntent.setAction(Intent.ACTION_PICK);
        addrIntent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);

        if(addrIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(addrIntent, ADDR_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String emailAddress;
        if(requestCode == ADDR_CODE && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();

            String[] projection = new String[1];
            projection[0] = ContactsContract.CommonDataKinds.Email.ADDRESS;
            Cursor cursor = getContentResolver().query(
                    contactUri, projection, null, null, null);
            if(cursor != null) {
                cursor.moveToFirst();
                emailAddress = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)
                );
                Log.d("DEBUG", emailAddress);
                EditText editText = findViewById(R.id.editText);
                editText.setText(emailAddress);
            }

        }
    }

    public void sendEmail(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");

        /* get the e-mail address from the EditText*/
        EditText editText = findViewById(R.id.editText);
        String emailAddress = editText.getText().toString();
        String[] addrTo = new String[1];
        addrTo[0] = emailAddress;

        /* set the intent extra */
        intent.putExtra(Intent.EXTRA_EMAIL, addrTo);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Hello");
        intent.putExtra(Intent.EXTRA_TEXT, "Hello, how are you?");

        /* try to launch the intent */
        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.w("PJDM", "No activity for e-mail");
        }


    }
}













